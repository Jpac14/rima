const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin")

module.exports = {
  webpack: {
    configure: {
      resolve: {
        plugins: [new TsconfigPathsPlugin()],
      },
    },
  },
}
